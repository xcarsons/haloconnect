//
//  User+CoreDataProperties.swift
//  HaloConnect
//
//  Created by Carson Schaefer on 1/14/16.
//  Copyright © 2016 CarsonSchaefer. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension User {

    @NSManaged var username: String?
    @NSManaged var email: String?
    @NSManaged var password: String?
    @NSManaged var xuid: String?
    @NSManaged var gamertag: String?

}
