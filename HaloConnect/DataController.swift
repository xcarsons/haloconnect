//
//  DataController.swift
//  HaloConnect
//
//  Responsible for CRUD operations on Core Data
//  Store Users information in the User entity object in model
//
//  Created by Carson Schaefer on 1/14/16.
//  Copyright © 2016 CarsonSchaefer. All rights reserved.
//

import UIKit
import CoreData

class DataController: NSObject {
    
    private let managedObjectContext: NSManagedObjectContext
    
    override  init() {

        // This resource is the same name as your xcdatamodeld contained in your project.
        guard let modelURL = NSBundle.mainBundle().URLForResource("CoreData", withExtension:"momd") else {
            fatalError("Error loading model from bundle")
        }
        // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
        guard let mom = NSManagedObjectModel(contentsOfURL: modelURL) else {
            fatalError("Error initializing mom from: \(modelURL)")
        }
        let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
        self.managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        self.managedObjectContext.persistentStoreCoordinator = psc

        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        let docURL = urls[urls.endIndex-1]
        /* The directory the application uses to store the Core Data store file.
        This code uses a file named "CoreData.sqlite" in the application's documents directory.
        */
        let storeURL = docURL.URLByAppendingPathComponent("CoreData.sqlite")
        do {
            try psc.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: storeURL, options: nil)
        } catch {
            fatalError("Error migrating store: \(error)")
        }
    }
    
    
    // Store the user information into CoreData
    class func storeUserInfo(username:String, password:String, email:String, XUID:String, gamertag:String) {
        let managedObjectContext = DataController().managedObjectContext
        let entity = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext: managedObjectContext) as! User
        if (!username.isEmpty) {
            entity.setValue(username.lowercaseString, forKey: "username")
        }
        if (!password.isEmpty) {
            entity.setValue(password, forKey: "password")
        }
        if (!email.isEmpty) {
            entity.setValue(email.lowercaseString, forKey: "email")
        }
        if (!XUID.isEmpty) {
            entity.setValue(XUID, forKey: "xuid")
        }
        if (!gamertag.isEmpty) {
            entity.setValue(gamertag, forKey: "gamertag")
        }
        
        do {
            try managedObjectContext.save()
        } catch {
            fatalError("failure to save context: \(error)")
        }
    }
    
    
    // Get the stored user information from CoreData
    class func fetch() -> [User] {
        let userFetch = NSFetchRequest(entityName: "User")
        let managedObjectContext = DataController().managedObjectContext
        do {
            let fetchedUser = try managedObjectContext.executeFetchRequest(userFetch) as! [User]
            return fetchedUser
        } catch {
            fatalError("failure to fetch user: \(error)")
        }
    }
    
    
    // Delete the stored user in Core Data
    // if information is provided in parameters, delete that specific user
    // if @all is true, delete all the users in Core Data
    class func deleteCoreData(username:String, email:String, all:Bool) {
        let managedObjectContext = DataController().managedObjectContext
        let users = getAllUsers(managedObjectContext)
        if (all) {
            for user in users {
                managedObjectContext.deleteObject(user)
            }
        } else {
            managedObjectContext.deleteObject(users.first!)
        }
        //eventually user will be allowed to have multiple gamertags
//        else if (username.lowercaseString.isEmpty)

        do {
            try managedObjectContext.save()
        } catch {
            fatalError("Failure to delete user: \(error)")
        }
    }
    
    
    // Update the stored user in Core Data
    // @username - select User with this username in Core Data to be updated
    // @email - select User with this email in Core Data to be updated
    // @change - select this attribute to be changed (username, gamertag, XUID....)
    // @val - the new value for the selected attribute
    // @context - Pass the current object context, default is set to class
    class func updateCoreData(username:String, email:String, change:String, val:String, context:NSManagedObjectContext = DataController().managedObjectContext) {
        let managedObjectContext = context
        var user:User
        if (!username.isEmpty) {
            user = findUser(managedObjectContext, attr: "username", val: username)!
        } else {
            user = findUser(managedObjectContext, attr: "email", val: email)!
        }
        
        user.setValue(val, forKey: change)
        do {
            try managedObjectContext.save()
        } catch {
            fatalError("Failure to update user: \(error)")
        }
    }
    
    
    // Retrun a User record found in Core Data with a specific attribute value
    // @context - Pass the current object context, default is set to class
    // @attr - (username, gamertag, email, xuid....)
    // @val  - The selected attr value
    class func findUser(context:NSManagedObjectContext = DataController().managedObjectContext, attr:String, val:String) -> User? {
        var value = val
        let managedObjectContext = context
        let entity = NSEntityDescription.entityForName("User", inManagedObjectContext: managedObjectContext)
        let request = NSFetchRequest()
        request.entity = entity
        if (attr.lowercaseString=="username" || attr.lowercaseString=="email") {
            value = val.lowercaseString
        }
        let condition = NSPredicate(format: attr+"=%@", value)
        request.predicate = condition
        
        do {
            let result = try managedObjectContext.executeFetchRequest(request)
            if (!result.isEmpty) {
                return result.first as? User
            }
        } catch let error as NSError {
            print("findUser error: \(error)")
        }
        return nil
    }
    
    
    // Return an array of Users stored in Core Data
    // @context - Pass the current object context, default is set to class
    class func getAllUsers(context:NSManagedObjectContext = DataController().managedObjectContext) -> Array<User> {
        var fetchedResults:Array<User> = Array<User>()
        let managedObjectContext = context//DataController().managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "User")
        
        //Execute Fetch request
        do {
            fetchedResults = try  managedObjectContext.executeFetchRequest(fetchRequest) as! [User]
        } catch let fetchError as NSError {
            print("retrieveById error: \(fetchError.localizedDescription)")
            fetchedResults = Array<User>()
        }
        
        return fetchedResults
    }
    
    
}
