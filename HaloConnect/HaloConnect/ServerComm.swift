//
//  ServerComm.swift
//  HaloConnect
//
//  Class responsible for communicating with the server.
//
//  Created by Carson Schaefer on 1/11/16.
//  Copyright © 2016 CarsonSchaefer. All rights reserved.
//

import Foundation

class ServerComm: NSObject {
    
    // create a user in the database, Post request with data.
    // returns the echo with a completion handler.
    class func createUser(username:String, password:String, email:String, completion:((phpResponse:String?)->Void)) {
        let request = NSMutableURLRequest(URL: NSURL(string: "http://hendrixserver.mynetgear.com:1111/haloconnect/userRegister.php")!)
        request.HTTPMethod = "POST"
        let postString = "username="+username.lowercaseString+"&password="+password+"&email="+email.lowercaseString
        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            let phpResponse = responseString! as String

            completion(phpResponse: phpResponse) // Completion handler return phpResponse
        }
        task.resume()
    }
    
    
    // Login the user in the database, Post request with data.
    // Returns the echo with a completion handeler.
    class func login(username:String, password:String, email:String, completiong:((phpResponse:String?)-> Void)) {
        
    }
    
    
    // Get gamertag verification code from database
    class func getGTVerificationCode(username:String, completion:(phpResponse:String?)->Void) {
        let request = NSMutableURLRequest(URL: NSURL(string: "http://hendrixserver.mynetgear.com:1111/haloconnect/getGamertagVerificationCode.php")!)
        request.HTTPMethod = "POST"
        let postString = "username="+username.lowercaseString
        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)            
            let phpResponse = responseString! as String
            
            completion(phpResponse: phpResponse) // Completion handler return phpResponse
        }
        task.resume()
    }
    
    
    // Send message containing gamertag verification code to entered gamertag
    class func sendGTVerificationCode(gamertagXUID:String, code:String) {
        let request = NSMutableURLRequest(URL: NSURL(string: "https://xboxapi.com/v2/messages")!)
        request.HTTPMethod = "POST"
        request.addValue("29580a1c686a253a774559510fa47442dc13d611", forHTTPHeaderField: "X-AUTH")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let postString = "{\"to\": [\""+gamertagXUID+"\"], \"message\": \"Halo Connect Verification Code: "+code+"\"}"
        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            //let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            //let phpResponse = responseString! as String
            
            //completion(phpResponse: phpResponse) // Completion handler return phpResponse
        }
        task.resume()
        
        
    }
    
    // returns corresponding XUID (for the xbox api) for the gamertag supplied
    // completion handler returns @xboxApiResponse
    class func getGamertagXUID(gamertag:String, completion:((xboxApiResponse:String?)->Void)) {
        let request = NSMutableURLRequest(URL: NSURL(string: "https://xboxapi.com/v2/xuid/"+gamertag)!)
        request.HTTPMethod = "GET"
        request.addValue("29580a1c686a253a774559510fa47442dc13d611", forHTTPHeaderField: "X-AUTH")
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            let xboxApiResponse = responseString! as String

            completion(xboxApiResponse: xboxApiResponse) // Completion handler return xboxApiResponse
        }
        task.resume()
    }

    
    // Resets the Verification code for the user
    // This new code is also returned from the database
    class func resetVerificationCode(username:String, completion:((phpResponse:String?)->Void)) {
        let request = NSMutableURLRequest(URL: NSURL(string: "http://hendrixserver.mynetgear.com:1111/haloconnect/resetVerificationCode.php")!)
        request.HTTPMethod = "POST"
        let postString = "username="+username.lowercaseString
        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            guard error == nil && data != nil else {
                // check for fundamental networking error
                print("error=\(error)")
                return
            }
        
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {
                // check for http errors
                print("statusCode shoudld be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)    
            let phpResponse = responseString! as String
            
            completion(phpResponse: phpResponse) // Completion handler return phpResponse
        }
        task.resume()
    }
    
    
    // Successful gamertag verification
    // Update records in the database, gamertag is now registered with user
    class func gamertagVerifySuccess(username:String, gamertagXUID:String, gamertag:String, completion:((phpResponse:String?)->Void)) {
        let request = NSMutableURLRequest(URL: NSURL(string: "http://hendrixserver.mynetgear.com:1111/haloconnect/gamertagVerifySuccess.php")!)
        request.HTTPMethod = "POST"
        let postString = "username="+username.lowercaseString+"&gamertagXUID="+gamertagXUID+"&gamertag="+gamertag.lowercaseString
        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            guard error == nil && data != nil else {
                // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {
                // check for http errors
                print("statusCode shoudld be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            let phpResponse = responseString! as String

            completion(phpResponse: phpResponse) // Completion handler return phpResponse
        }
        task.resume()
    }
    
}
