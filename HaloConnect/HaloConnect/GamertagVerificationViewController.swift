//
//  GamertagVerificationViewController.swift
//  HaloConnect
//
//  Created by Carson Schaefer on 1/12/16.
//  Copyright © 2016 CarsonSchaefer. All rights reserved.
//

import UIKit

class GamertagVerificationViewController: UIViewController {
    
    @IBOutlet weak var gamertagTB: UITextField!
    @IBOutlet weak var verificationCodeTB: UITextField!
    @IBOutlet weak var gamertagLbl: UILabel!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var indicateLbl: UILabel!
    @IBOutlet weak var resetBtn: UIButton!
    
    var code:String = ""
    var gamertagXUID:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation
    @IBAction func SubmitVerificationCode(sender: AnyObject) {
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // display an alert message to user
    // message: what the issue is
    func infoErrorAlert(message:String) {
        let alertController = UIAlertController(title: "Info Error", message: message, preferredStyle: UIAlertControllerStyle.Alert);
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    // Modify progressView
    // This is the activity indicator with text = @message
    func displayActivityIndicator(message:String, hide:Bool) {
        indicateLbl.text = message
        progressView.hidden = hide;
    }
    

    // Register button pressed,
    @IBAction func RegisterGamertag(sender: AnyObject) {
        displayActivityIndicator("Sending Code", hide: false)
        gamertagLbl.text = gamertagTB.text
        let username = DataController.fetch().first?.username

        // Get the gamertag XUID from the Xbox API
        ServerComm.getGamertagXUID(gamertagTB.text!) {
            xboxApiResponse in
            dispatch_async(dispatch_get_main_queue()) { // this is executed once completion handler is called
                self.gamertagXUID = xboxApiResponse!
                print(self.gamertagXUID)
                // retrieve the verification code for the signed up user from the db
                ServerComm.getGTVerificationCode(username!) {
                    phpResponse in
                    dispatch_async(dispatch_get_main_queue()) {
                        self.code = phpResponse!
                        // send the verification code to the gamertag in a message
                        ServerComm.sendGTVerificationCode(self.gamertagXUID, code: self.code)
                        self.displayActivityIndicator("", hide: true)
                    }
                }
            }
        }
    }
    
    
    // Reset code button pressed
    @IBAction func ResetVerificationCode(sender: AnyObject) {
        displayActivityIndicator("Resetting Code", hide: false)
        let username = DataController.fetch().first?.username
        // reset the verification code and it
        ServerComm.resetVerificationCode(username!) {
            phpResponse in
            dispatch_async(dispatch_get_main_queue()) {
                self.code = phpResponse!
                self.displayActivityIndicator("", hide: true)
                // send the new verification code to the gamertag in a message
                ServerComm.sendGTVerificationCode(self.gamertagXUID, code: self.code)
                self.displayActivityIndicator("", hide: true)
            }
        }
    }

    
    // Submit verification code button pressed
    @IBAction func SubmitVerificationCode(sender: AnyObject) {
        displayActivityIndicator("Verifying", hide: false)
        
        if (verificationCodeTB.text == self.code) {
            let username = DataController.fetch().first?.username
            let gamertag = gamertagLbl.text
            let gamertagXUID = self.gamertagXUID
            // submit the information, gamertag verified
            ServerComm.gamertagVerifySuccess(username!, gamertagXUID: gamertagXUID, gamertag: gamertag!) {
                phpResponse in
                dispatch_async(dispatch_get_main_queue()) {
                    if (!(phpResponse!.lowercaseString=="success")) {
                        print("problem updating verified user: "+phpResponse!)
                    } else {
                        //update core data with gamertag and xuid
                        DataController.updateCoreData(username!, email: "", change: "gamertag", val: gamertag!)
                        DataController.updateCoreData(username!, email: "", change: "xuid", val: gamertagXUID)
                        // transition to User Home View
                        let storyboard = UIStoryboard(name: "UserMain", bundle: nil)
                        // GamertagVeri.... This needs to be the navigation storyboard ID
                        let vc = storyboard.instantiateViewControllerWithIdentifier("UserHomeViewController") as UIViewController
                        self.presentViewController(vc, animated: true, completion: nil)
                    }
                }
            }
        } else {
            displayActivityIndicator("", hide: true)
            infoErrorAlert("Code is Incorrect")
            resetBtn.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
        }
        
        
    }

}
