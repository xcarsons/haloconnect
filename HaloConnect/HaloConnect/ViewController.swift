//
//  ViewController.swift
//  HaloConnect
//
//  Created by Carson Schaefer on 1/8/16.
//  Copyright © 2016 CarsonSchaefer. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    @IBOutlet weak var LoginBtn: UIButton!
    @IBOutlet weak var usernameTb: UITextField!
    @IBOutlet weak var passwordTb: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        DataController.deleteCoreData("",email: "",all: true)
        DataController.storeUserInfo("Bob", password: "BOB", email: "BOB@aol.com", XUID: "", gamertag: "")
        print("Inserted "+(DataController.fetch().first?.username)!)
        DataController.updateCoreData("", email: "bob@aol.com", change: "username", val: "Tim")
        print("Updated Bob, now "+(DataController.fetch().first?.username)!)
        DataController.deleteCoreData("",email: "",all: true)

        
        
        // hide navigation bar
        self.navigationController?.navigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //Login Button Pressed, send information.
    @IBAction func login(sender: AnyObject) {
        
    }
    
    //Register Button pressed, move to RegisterScene and make nav bar visible
    @IBAction func registerView(sender: AnyObject) {
        self.navigationController?.navigationBarHidden = false
    }
    
    // Login button pressed, log the user in
    @IBAction func loginUser(sender: AnyObject) {
        
    }
    

}

