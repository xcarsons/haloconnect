//
//  UserHomeViewController.swift
//  HaloConnect
//
//  Created by Carson Schaefer on 1/22/16.
//  Copyright © 2016 CarsonSchaefer. All rights reserved.
//

import UIKit

class UserHomeViewController: UIViewController {
    //label for user info
    
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var xuidLbl: UILabel!
    @IBOutlet weak var gamertagLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // core data for current user
        let coredata:User = DataController.fetch().first!
        
        usernameLbl.text = coredata.username
        xuidLbl.text = coredata.xuid
        gamertagLbl.text = coredata.gamertag
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
