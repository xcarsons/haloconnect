//
//  RegisterViewController.swift
//  HaloConnect
//
//  Created by Carson Schaefer on 1/9/16.
//  Copyright © 2016 CarsonSchaefer. All rights reserved.
//

import UIKit
import CoreData

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var usernameTb: UITextField!
    @IBOutlet weak var emailTb: UITextField!
    @IBOutlet weak var password1Tb: UITextField!
    @IBOutlet weak var password2Tb: UITextField!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var indicateLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        progressView.layer.cornerRadius = 15
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // display an alert message to user
    // message: what the issue is
    func infoErrorAlert(message:String) {
        let alertController = UIAlertController(title: "Info Error", message: message, preferredStyle: UIAlertControllerStyle.Alert);
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    // Modify progressView, loading spinner
    // This is the activity indicator with text = @message
    func displayActivityIndicator(message:String, hide:Bool) {
        indicateLbl.text = message
        progressView.hidden = hide;
    }
    
    
    // create user button pressed
    @IBAction func createAccount(sender: AnyObject) {
        // make sure information is supplied
        if (usernameTb.text!.isEmpty || emailTb.text!.isEmpty || password1Tb.text!.isEmpty || password2Tb.text!.isEmpty) {
            infoErrorAlert("Fields cannot be left empty.")
            return
        }
        
        // make sure username doesn't contain symbols
        if (usernameTb.text!.containsString("@")) {
            infoErrorAlert("Enter a valid username. (no symbols)")
            return
        }
        
        // check if a valid email is entered
        if (!emailTb.text!.containsString("@")) {
            infoErrorAlert("Enter a valid email address.")
            return
        }
        
        // check if passwords match
        if (password1Tb.text != password2Tb.text) {
            infoErrorAlert("Passwords do not match.")
            return
        }
        
        displayActivityIndicator("Registering", hide: false)
        
        // Has completion handler return the php echo as phpResponse
        ServerComm.createUser(self.usernameTb.text!, password: self.password1Tb.text!, email: self.emailTb.text!) {
            phpResponse in
            dispatch_async(dispatch_get_main_queue()) { // this is executed once completion handler is called
                if (!phpResponse!.containsString("Success")) {
                    self.displayActivityIndicator("", hide: true)
                    self.infoErrorAlert(phpResponse!)
                } else {
                    // Store the username, password, and email in core data
                    DataController.storeUserInfo(self.usernameTb.text!, password: self.password1Tb.text!, email: self.emailTb.text!, XUID: "", gamertag: "")
                    
                    self.displayActivityIndicator("", hide: true)
                    let storyboard = UIStoryboard(name: "UserMain", bundle: nil)
                    // GamertagVeri.... This needs to be the navigation storyboard ID
                    let vc = storyboard.instantiateViewControllerWithIdentifier("GamertagVerificationViewController") as UIViewController
                    self.presentViewController(vc, animated: true, completion: nil)
                }
            }
        }
        
    }
    

}
